"""
Module docstring describing the purpose of the file.
"""

from hats_rest.views import api_list_hats, api_show_hats
from django.urls import path

urlpatterns = [
    path("hats/", api_list_hats, name="api_list_hats"),
    path("hats/<int:pk>/", api_show_hats, name="api_show_hats")
]
