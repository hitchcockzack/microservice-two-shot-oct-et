from django.contrib import admin
from .models import Hat, LocationVO


@admin.register(Hat)
class HatAdmin(admin.ModelAdmin):
    list_display = [
        "fabric",
        "style_name",
        "color",
    ]
@admin.register(LocationVO)
class LocationVOAdmin(admin.ModelAdmin):
    list_display = [
        "closet_name",
        "section_number",
        "shelf_number",
    ]
