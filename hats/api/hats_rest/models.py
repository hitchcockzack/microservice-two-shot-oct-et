from django.db import models
from django.urls import reverse

class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True, null=True)
    closet_name = models.CharField(max_length=200, null=True)
    section_number = models.PositiveSmallIntegerField(null=True)
    shelf_number = models.PositiveSmallIntegerField(null=True)


class Hat(models.Model):
    id= models.PositiveSmallIntegerField(unique=True, primary_key=True)
    fabric = models.CharField(max_length=50)
    style_name = models.CharField(max_length=50)
    color = models.CharField(max_length=50)
    URL = models.URLField(unique=False)
    location = models.ForeignKey(
        LocationVO,
        related_name="location",
        on_delete=models.CASCADE,
    )


    def __str__(self):
        return self.name


    class Meta:
        ordering = ("fabric", "style_name", "color", "URL", "id")
