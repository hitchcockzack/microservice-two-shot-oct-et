from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import LocationVO, Hat
from .models import Hat


class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href",
        "closet_name",
        "section_number",
        "shelf_number",
    ]


class Detail_Hat(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "URL",
        "location",
    ]
    encoders = {
        "location": LocationVOEncoder(),
    }


class List_Hat(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "fabric",
        "style_name",
        "color",
        "URL",
    ]


@require_http_methods(["GET", "POST"])
def api_list_hats(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is None:
            hats = Hat.objects.all()
        else:
            hats = Hat.objects.filter(location=location_vo_id)
        return JsonResponse(
            {"hats": hats},
            encoder=List_Hat
        )
    else:
        content = json.loads(request.body)
        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "invalid id"},
                status=404
            )
    hat = Hat.objects.create(**content)
    return JsonResponse(
        hat,
        encoder=Detail_Hat,
        safe=False,
    )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_hats(request, pk):
    if request.method == "GET":
        hats = Hat.objects.get(id=pk)
        return JsonResponse(
            hats,
            encoder=Detail_Hat,
            safe=False
        )
    elif request.method == "DELETE":
        try:
            hat = Hat.objects.get(id=pk)
            hat.delete()
            return JsonResponse({"message": "deleted"}, safe=False)
        except Hat.DoesNotExist:
            return JsonResponse({"message": "invalid id"})
    else:
        content = json.loads(request.body)
        try:
            if "hats" in content:
                hats = Hat.objects.get(id=pk)
                hats.fabric = content["fabric"]
                hats.style_name = content["style_name"]
                hats.color = content["color"]
                hats.URL = content["URL"]
                hats.location = content["location"]
                hats.save()
        except Hat.DoesNotExist:
            return JsonResponse(
                {"message": "invalid id"},
                status=404)
        return JsonResponse(
            Hat,
            encoder=Detail_Hat,
            safe=False,
        )
