# Wardrobify

Team:

* Jim Floyd - Hats
* Zack Hitchcock - Shoes

## Design
the design for this project was minimal, to keep things clean and easy to grade.
bootstrap css was used for the look and feel, with some minor tweaks to the buttons, headers, and tables.

## How to Run:
To run this project - fork and clone repository from gitlab: https://gitlab.com/hitchcockzack/microservice-two-shot-oct-et

in the new repositorys local directory, (with docker installed) run the following commands: <br>
```
docker volume create <chosen_volume_name>
docker-compose build
docker-compose up
```
once the docker environment is running, you should successfully have access to our two-shot microservice project. features are down below.

## Shoes microservice

in the shoes microservice, i decided to begin with the backend. first up was the models, adding the properties mentioned in the rubric. "bin", is a foreignkey to BinVO, a model i created to clone the Bin object properties in the wardrobe api. the poller, which i created next, polls the data from the wardrobe api and imports it into an identical BinVO object, every 60 seconds. Shoe bins are populated from BinVO objects.

available HTTP actions for my microservice are GET, POST, and DELETE. GET & POST function together in a view, with DELETE being a separate view function altogether. the reason for this is we are not showing shoe details separately, and i needed an integer value to call with the DELETE request in my api. with that, you can show or create a shoe, and if any need to be removed, send a delete request to that particular shoe, which will return:

```
{
	"shoe- <int:pk>": "has been deleted"
}
```


Shoes Microservice runs on port 8080 in the docker container.



### Shoes Backend API calls: 
List Shoes: **GET http://localhost:8080/api/shoes/** <br>
Create Shoes: **POST http://localhost:8080/api/shoes/** <br>

(takes JSON input in this format)
```
{
	"id":"1",
	"model_name": "air force",
	"manufacturer": "nike",
	"color": "white",
	"bin": "/api/bins/<int:pk>/", [^1]
	"img": "https://static.nike.com/a/images/t_PDP_864_v1/f_auto,b_rgb:f5f5f5/b7d9211c-26e7-431a-ac24-b0540fb3c00f/air-force-1-07-mens-shoes-jBrhbr.png"
}
```


Delete Shoes: **DELETE http://localhost:8080/api/shoes/`<int:pk>`/** <br>


### Shoes Frontend API calls:
Main page: http://localhost:3000/ <br>
List of shoes: http://localhost:3000/shoes <br>
Create a Shoe: http://localhost:3000/newShoe <br>

Also, the frontend nav functions normally and should bring you there anyways.

[^1] "bin" accepts path in insomnia, but the form is populated with closet_name for ease of use


## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.

In the hats microservice I created two models, Hat and LocationVO. I incorporated fields that best represent these models including: fabric, style name, color, picture for Hats and closet name, section number, and shelf number. The Hat model uses the location API to represent the location.

In the hats views I used model encoders to display model properties. The views allow for GET, POST, DELETE, and PUT requests.

Available API calls:
GET hat list "http://localhost:8090/api/hats/"
POST hat list "http://localhost:8090/api/hats/"
DEL hat list "http://localhost:8090/api/hats/1/"
POST location "http://localhost:8100/locations"
