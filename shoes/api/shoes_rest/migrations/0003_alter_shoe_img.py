# Generated by Django 4.0.3 on 2023-12-13 20:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shoes_rest', '0002_alter_shoe_options_shoe_bin'),
    ]

    operations = [
        migrations.AlterField(
            model_name='shoe',
            name='img',
            field=models.URLField(),
        ),
    ]
