from django.urls import path
from .views import api_list_shoes, api_shoe_delete

urlpatterns = [
    path("shoes/", api_list_shoes, name="api_list_shoes"),
    path("shoes/<int:pk>/", api_shoe_delete, name="api_shoe_delete"),
]
