from django.shortcuts import render
from .models import Shoe, BinVO
from common.json import ModelEncoder
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
# Create your views here.

class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = ["closet_name", "bin_number", "bin_size", "import_href"]


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "model_name",
        "manufacturer",
        "color",
        "bin",
        ]
    encoders={
        "bin": BinVOEncoder()
    }


@require_http_methods(["DELETE"])
def api_shoe_delete(request, pk):
    if request.method == "DELETE":
        try:
            shoe = Shoe.objects.get(id=pk)
            shoe.delete()
            delete_message = {f"shoe- {pk}":"has been deleted"}
            return JsonResponse(delete_message,
                                safe=False)
        except Shoe.DoesNotExist:
            return JsonResponse({"shoe": "Does not exist"},
                                safe=False)


@require_http_methods(["GET", "POST", ])
def api_list_shoes(request):
    if request.method == "GET":
        shoe = Shoe.objects.all()
        print(shoe)
        return JsonResponse(
            {"shoes": shoe},
            encoder=ShoeListEncoder,
        )

    else:
        content = json.loads(request.body)

        # Get the bin object and put it in the content dict
        try:
            shoe_href = content["bin"]
            bin = BinVO.objects.get(import_href=shoe_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )

        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeListEncoder,
            safe=False,
        )
