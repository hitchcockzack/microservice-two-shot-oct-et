from django.db import models
from django.urls import reverse
"branch test"

# Create your models here.
class BinVO(models.Model):
    closet_name = models.CharField(max_length=100)
    bin_number = models.PositiveSmallIntegerField()
    bin_size = models.PositiveSmallIntegerField()
    import_href = models.CharField(max_length=200, unique=True, null=True)






class Shoe(models.Model):
    id = models.PositiveIntegerField(unique=True, primary_key=True)
    color = models.CharField(max_length=100)
    manufacturer = models.CharField(max_length=100)
    model_name = models.CharField(max_length=100)
    img = models.URLField(unique=False)
    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.CASCADE,
        null=True
        )


    def __str__(self):
        return f"{self.id}, {self.manufacturer}, {self.model_name}"

    def get_api_url(self):
        return reverse("api_list_shoes", kwargs={"id": self.id})

    class Meta:
        ordering = ("id", "manufacturer", "model_name")
