import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import NewShoeForm from "./ShoeForm";

function ShoesList() {
    const [shoes, setShoes] = useState([]);
    const navigate = useNavigate()
    const fetchShoes = async () => {
        const shoesApiUrl = "http://localhost:8080/api/shoes/";
        const response = await fetch(shoesApiUrl);
        if (response.ok) {
            const data = await response.json();
            console.log(data)

            if (data === undefined) {
                return null;
            }

            setShoes(data.shoes)
        }
    }
    useEffect(() => {
        fetchShoes();
    }, [])

    const handleOnClick = () => {
    navigate("/newShoe");
}

    const handleDelete = async (id) => {
        console.log(id)
        const confirmDelete = window.confirm('Are you sure you want to delete this entry?');
        if (confirmDelete) {
          try {
            const request = await fetch(`http://localhost:8080/api/shoes/${id}/`, { method: "DELETE" });
            const response = await request.json();
            fetchShoes();
            window.alert('shoe has been deleted');
          } catch (error) {
            console.error("shoe cant be deleted")
          }
        }
      }

    return (
    <>
        <table className="table table-striped">
        <thead>
          <tr>
            <th>Model</th>
            <th>Manufacturer</th>
            <th>Color</th>
            <th>Bin</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
          {shoes.map(shoe => {
            return (
              <tr key={ shoe.id }>
                <td>{ shoe.model_name }</td>
                <td>{ shoe.manufacturer }</td>
                <td>{ shoe.color }</td>
                <td>{ shoe.bin.closet_name }</td>
                <td><button onClick={() => {handleDelete(shoe.id)}}>Delete</button></td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <div><button onClick={handleOnClick} type="button" className='btn btn-primary' data-bs-toggle="button">create a new shoe?</button></div>
      </>
    );
  }

export default ShoesList;
