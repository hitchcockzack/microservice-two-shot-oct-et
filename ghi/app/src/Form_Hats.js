
import React, { useState, useEffect } from 'react';


function Hats_Form() {
    const [fabric, setFabric] = useState('')
    const [style_name, setStyleName] = useState('')
    const [color, setColor] = useState('')
    const [URL, setUrl] = useState('')
    const [id, setId] = useState('')
    const [location, setLocation]= useState([])
    const [locations, setLocations]= useState([])
    const fetchData = async () => {
        const locationURL = "http://localhost:8100/api/locations/";
        const response = await fetch(locationURL);
        if (response.ok) {
            const data = await response.json()
            setLocations(data.locations);
        }
    }
    useEffect(() => {
        fetchData();
    }, []);


    const handleFabricChange = async (event) => {
        const value = event.target.value
        setFabric(value)
    }
    const handleStyleNameChange = async (event) => {
        const value = event.target.value
        setStyleName(value)
    }
    const handleColorChange = async (event) => {
        const value = event.target.value
        setColor(value)
    }

    const handleLocationChange = async (event) => {
        const value = event.target.value
        setLocation(value)
    }

    const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
        data.fabric = {fabric};
        data.style_name = {style_name};
        data.color = {color};
        data.URL = {URL};
        data.location = {location};
    console.log(data)

    const jsonData = JSON.stringify(data)
    const HatDictURL = "http://localhost:8090/api/hats/"
    const fetchConfig = {
        method: "post",
        body: jsonData,
        headers: {
            'Content-Type': 'application/json',
    }
}

    const response = await fetch(HatDictURL, fetchConfig);
    if (response.ok) {
        const addHats = await response.json();
        console.log(addHats);

        setFabric("");
        setStyleName("");
        setColor("");
        setUrl("");
        setLocation("");
    }
    }


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a Hat</h1>
                    <form onSubmit={handleSubmit} id="create-hat-form">

                    <div className="form-floating mb-3">
                            <input onChange={handleFabricChange} placeholder="fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                            <label htmlFor="fabric">Enter Fabric Here!</label>
                        </div>

                        <div className="form-floating mb-3">
                            <input onChange={handleStyleNameChange} placeholder="Name" required type="text" name="name" id="style name" className="form-control" />
                            <label htmlFor="stylename">Enter Style Name Here!</label>
                        </div>

                        <div className="form-floating mb-3">
                            <input onChange={handleColorChange} placeholder="color" required type="text" name="color" id="color" className="form-control" />
                            <label htmlFor="colorname">Enter Color Here!</label>
                        </div>

                        {/* <div className="form-floating mb-3">
                            <input onChange={handleURLChange} placeholder="URL" required type="url" name="url" id="url" className="form-control" />
                            <label htmlFor="url">url</label>
                        </div> */}

                        <div className="mb-3">
                            <select onChange={handleLocationChange} required name="location" id="location" className="form-select">
                                <option value="">Choose a location</option>
                                {locations.map(location => {
                                    return (
                                        <option key={location.id} value={location.id}>{location.name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>

                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default Hats_Form;
