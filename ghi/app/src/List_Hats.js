import React from 'react';


function List_Hats(props) {
    return (
    <table className="table table-striped">
        <thead>
        <tr>
            <th>Name</th>
            <th>Picture</th>
        </tr>
        </thead>
        <tbody>
        {props.hat.map(hats => {
            return (
            <tr key={hats.href}>
                <td>{ hats.stylename }</td>
                <td>{ hats.fabric }</td>
                <td>{ hats.color }</td>
                <td><img src={hats.url} /></td>
            </tr>
            );
        })}
        </tbody>
    </table>
    );
}

export default List_Hats;
