import React, {useState, useEffect} from 'react';

const Form = {
    id: '',
    model_name: '',
    manufacturer: '',
    color: '',
    bin: '',
    img:'',
};

function NewShoeForm () {
    const [bins, setBins] = useState([]);
    const [formData, setFormData] = useState({ ...Form });

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setBins(data.bins);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);



    const handleInputChange = (e) => {
        const inputName = e.target.name;
        const value = e.target.value;

        setFormData({
        ...formData,
        [inputName]: value
        });
    }

    const handleSubmit = async (e) => {
        e.preventDefault();

        const form = formData;
        const url = `http://localhost:8080/api/shoes/`;

        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch (url, fetchConfig);
        console.log(response)
        if (response.ok) {
            setFormData({ ...Form })
        }
    };

    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a New Shoe</h1>
            <form onSubmit={handleSubmit} id="aff-shoe-form">
            <div className="form-floating mb-3">
                <input onChange={handleInputChange} value={formData.id} placeholder="ID" required type="text" name="id" id="id" className="form-control" />
                <label htmlFor="name">ID</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleInputChange} value={formData.model_name} placeholder="Name" required type="text" name="model_name" id="model_name" className="form-control" />
                <label htmlFor="name">Model Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleInputChange} value={formData.manufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                <label htmlFor="manufacturer">Manufacturer</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleInputChange} value={formData.color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleInputChange} value={formData.img} placeholder="URL" required type="Url" name="img" id="img" className="form-control" />
                <label htmlFor="url">Image URL</label>
              </div>
              <div className="mb-3">
                <select onChange={handleInputChange} value={formData.bin} required name="bin" id="bin" className="form-select">
                  <option value="">Choose a Bin Location</option>
                  {bins.map(bin => {
                    return (
                      <option key={bin.href} value={bin.href}>{bin.closet_name}</option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );

                }
export default NewShoeForm;
