import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import Form_Hats from './Form_Hats';
import List_Hats from './List_Hats';
import ShoesList from './ShoesList';
import NewShoeForm from './ShoeForm';

function App() {
  return (

    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/hats"></Route>
          <Route path="" element={<Form_Hats/>}/>
          <Route path="new" element={<List_Hats/>}/>
          <Route path="shoes/" element={<ShoesList />} />
          <Route path="newShoe/" element={<NewShoeForm />} />
        </Routes>
      </div>
    </BrowserRouter>

  );
}

export default App;
